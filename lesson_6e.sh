#!/bin/bash -xv
# if statement exercise: Print the largest number of the 4 entered
# syntax

echo "numbers passed as arguments: "$@

largest_num=0

for num in $@
do
	if [ $num \> $largest_num ]
	then
		largest_num=$num
	fi

done

echo "Largest number is:" $largest_num
