#!/bin/bash

### Print total arguments and their values
# to count the total number of arguments use $#
# to display the total number of arguments use $@

echo "Total Arguments:" $#
echo "All Arguments values:" $@

### Command arguments can be accessed as
# to display the first entered use $1
# to display the second entered use $1

echo "First ->" $1
echo "Second ->" $2


