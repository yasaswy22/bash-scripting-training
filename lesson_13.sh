#!/bin/bash

#for variable in values; do
#condition;
#done

#for i in 1 2 3 4 5; do
#	echo "Number "$i
#done

# Can also mention range
# increment of 2 between 1 and 10
for i in {1..10..2};
do
	echo "Number $i"
done

#END=10
#for i in $(seq 1 $END);
#do
#	echo $i;
#done

for ((i=0;i<10;i++));
do
	echo "Number $i"
done
