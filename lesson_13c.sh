#!/bin/bash
value=0
mkdir /mnt/c/dev/shell/testfiles

for i in /mnt/c/dev/shell/*.pdf; do
	DUU=$(du -h "$i" | awk '{print$1}')
	if [ $DUU == $value ]
	then
		echo "0KB files $i"
		mv -f $i /mnt/c/dev/shell/testfiles
	fi
done
