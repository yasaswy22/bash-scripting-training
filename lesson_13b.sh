#!/bin/bash
for i in *.pdf; do
#	echo "i: $i"
#	echo "$(du -h $i)"
	size=$(du -h "$i" | awk '{print$1}')
	echo "File size: $size"
	name=$(du -h "$i" | awk '{print$2}')
	echo "File name: $name"
done
