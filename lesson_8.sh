#!/bin/bash
### File operators
#
# -f file exists
# -d dir exit or not
# -s file non empty file
# -d dir is directory exist and not a file
# -w file is writable file
# -r filr is read-only file
# -x file is file executable

echo "Enter file name: "
read file_name
if [ -f  $file_name ]
then 
	echo "File $file_name exists"
else
	echo "File $file_name does not exist "
fi
