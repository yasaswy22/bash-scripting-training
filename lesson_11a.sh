#!/bin/bash

#Arithmetic operators

echo "Enter first number:"
read num1
echo "Enter second number:"
read num2

#Only works for integers. Won't recognize the floating point numbers
echo "Addition:" $(( $num1 + $num2 ))
echo "Subtraction:" $(( $num1 - $num2 ))
echo "Multiplication:" $(( $num1 * $num2 ))
echo "Division:" $(( $num1 / $num2 ))
