#!/bin/bash -xv
# if statement exercise: Print the largest number of the 4 entered
# syntax

echo "numbers passed as arguments: "$@

num1=$1
num2=$2
num3=$3
num4=$4

if [ $num1 \> $num2 -a $num1 \> $num3 -a $num1 \> $num4 ];
then
echo "$num1 is the largest"
elif [ $num2 \> $num3 -a $num2 \> $num4 ];
then
echo "$num2 is the largest"
elif [ $num3 \> $num4 ];
then
echo "$num3 is the largest"
else
echo "$num4 is the largest"
fi
