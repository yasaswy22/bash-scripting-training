#!/bin/bash
echo "Enter SSC Percentage"
read ssc
echo "Enter Inter Percentage"
read inter

# -a is AND operator
#if [ $ssc -ge  70 -a $inter -ge 60 ]
if [ $ssc -ge 70 ] && [ $inter -ge 60 ]
then
	echo "Candidate is eligible"
else
	echo "Candidate is not eligible"
fi

# -o is OR operator
#if [ $ssc -ge  70 -o $inter -ge 60 ]
if [ $ssc -ge 70 ] || [ $inter -ge 60 ]
then
	echo "Candidate needs some training"
else
	echo "Candidate can't be trained"
fi
