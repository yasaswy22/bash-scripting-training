#!/bin/bash
# if statement
# syntax

echo "numbers passed as arguments: "$@

num1=$1
num2=$2

if [ $num1 \>  $num2 ]
then
	echo "First number: $num1 is greater than the second number: $num2"
elif [ $num1 \< $num2 ]
then
	echo "First number: $num1 is less than the second number: $num2"
else
	echo "First number: $num1 is equal to second number: $num2"
fi

