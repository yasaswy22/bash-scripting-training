#!/bin/bash -xv

# If else
#read -p "Enter the numeric value: " EnteredValue
#if [ $EnteredValue -gt 90 ]
#then 
#	echo "Entered value is greater than 90"
#else
#	echo "Entered value is less than 90"
#:fi

# Else if - for multiple conditions
# elif won't run if the if is true

read -p "Enter the numeric value: " EnteredValue
if [ $EnteredValue -gt 90 ]
then
	echo "Entered value is greater than 90"
elif [ $EnteredValue -ge 70 ]
then
	echo "Entered value is greater than 70"
else
	echo "Entered value is less than 70"
fi

